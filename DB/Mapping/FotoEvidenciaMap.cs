﻿using System;
using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppTUMISMITOPE.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AppTUMISMITOPE.DB.Mapping
{
    public class FotoEvidenciaMap : IEntityTypeConfiguration<FotoEvidencia>

    {
        public void Configure(EntityTypeBuilder<FotoEvidencia> builder)
        {


            builder.ToTable("FotoEvidencia", "dbo");
            builder.HasKey(FotoEvidencia => FotoEvidencia.id);


        }
    }
}
