﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AppTUMISMITOPE.Models;

namespace AppTUMISMITOPE.DB.Mapping
{
    public class TarjetaMap : IEntityTypeConfiguration<Tarjeta>

    {
        public void Configure(EntityTypeBuilder<Tarjeta> builder)
        {


            builder.ToTable("Tarjeta", "dbo");
            builder.HasKey(Tarjeta => Tarjeta.id);
            //builder.HasMany(Libro => Libro.bookAutores).WithOne().HasForeignKey(o => o.idBook);

        }
    }
}
