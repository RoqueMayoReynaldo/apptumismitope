﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.DB.Mapping;
namespace AppTUMISMITOPE.DB

{
    public class AppTumismitoPeContext:DbContext
    {
        public DbSet<Comentario> Comentarios { get; set; }
        public DbSet<Donacion> Donaciones { get; set; }
        public DbSet<PerfilProfesional> PerfilProfesionales { get; set; }
        public DbSet<Postulacion> Postulaciones { get; set; }
        public DbSet<Publicacion> Publicaciones { get; set; }
        public DbSet<ReaccionPublicacion> ReaccionPublicaciones { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Foto> Fotos { get; set; }
        public DbSet<ReplyComentario> ReplyComentarios { get; set; }
        public DbSet<Tarjeta> Tarjetas { get; set; }
        public DbSet<Caja> Cajas { get; set; }
        public DbSet<LikePostulacion> LikePostulaciones { get; set; }

        public DbSet<Evidencia> Evidencias { get; set; }
        public DbSet<FotoEvidencia> FotoEvidencias { get; set; }
        public AppTumismitoPeContext(DbContextOptions<AppTumismitoPeContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new ComentarioMap());
            modelBuilder.ApplyConfiguration(new DonacionMap());
            modelBuilder.ApplyConfiguration(new PerfilProfesionalMap());
            modelBuilder.ApplyConfiguration(new PostulacionMap());
            modelBuilder.ApplyConfiguration(new PublicacionMap());
            modelBuilder.ApplyConfiguration(new ReaccionPublicacionMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new FotoMap());
            modelBuilder.ApplyConfiguration(new ReplyComentarioMap());
            modelBuilder.ApplyConfiguration(new TarjetaMap());
            modelBuilder.ApplyConfiguration(new LikePostulacionMap());
            modelBuilder.ApplyConfiguration(new CajaMap());

            modelBuilder.ApplyConfiguration(new EvidenciaMap());
            modelBuilder.ApplyConfiguration(new FotoEvidenciaMap());
        }

    }
}
