﻿using AppTUMISMITOPE.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AppTUMISMITOPE.DB;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace AppTUMISMITOPE.Controllers
{
    public class WelcomeController : Controller
    {
        private AppTumismitoPeContext context;
        private IConfiguration configuration;
        //Inyeactamos ICONFIGURATION para poder usar el valor de la llave para la contraseña alojado en appsetings.json
        public WelcomeController(AppTumismitoPeContext context,IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }



        [HttpGet]
        public IActionResult SingIn()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SingIn(String username,String password)
        {

            Usuario user = context.Usuarios.FirstOrDefault(Usuario=>Usuario.userName==username && Usuario.pass==password);

            if (user == null)
            {
                //temp data solo existe durante un unico request
                TempData["MensajeLogeo"] = "Usuario y contraseña incorrectos";
                return RedirectToAction("SingIn");
            }
            else {
                var claims = new List<Claim> {
                new Claim(ClaimTypes.Name,username) };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                HttpContext.SignInAsync(claimsPrincipal);


                return RedirectToAction("Index", "Home");

            }

            
        }

        public string CrearHash(string pass)
        {
            pass = pass + configuration.GetValue<string>("Key");  //obtenemos el key desde appsetting.json,esto es muy util cuando este en produccion
            var sha = SHA512.Create();
            var bytes = Encoding.Default.GetBytes(pass);
            var hash = sha.ComputeHash(bytes);
            return Convert.ToBase64String(hash) ;
        }


    }
}
