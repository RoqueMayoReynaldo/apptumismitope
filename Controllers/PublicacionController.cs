﻿using AppTUMISMITOPE.DB;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppTUMISMITOPE.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace AppTUMISMITOPE.Controllers
{
    public class PubModel
    {
        public Publicacion pub { get; set; }
        public Usuario UsuarioActivo { get; set; }
        public DateTime fechaActual { get; set; }
    }

    public class PostulacionModel
    {
      
        public Usuario UsuarioActivo { get; set; }
        public DateTime fechaActual { get; set; }
    }





    [Authorize]
    public class PublicacionController : Controller
    {
        // GET: PublicacionController

        private AppTumismitoPeContext context;
        private IWebHostEnvironment hosting;

        public PublicacionController(AppTumismitoPeContext context,  IWebHostEnvironment hosting)
        {
            this.context = context;
            this.hosting= hosting;
        }

        [HttpGet]
        public ActionResult Detalle(int id)
        {
            actualizarPostulaciones();

            var claim = HttpContext.User.Claims.First();
            string conectUser = claim.Value;

            Usuario us = context.Usuarios.FirstOrDefault(o=>o.userName==conectUser);
            PubModel PubModel = new PubModel
            {
                pub = context.Publicaciones.Include("usuario").Include("fotos").Include("donaciones.usuario").Include("reacciones.usuario").Include("comentarios.usuario").Include("comentarios.replyComentarios.usuario").Include("postulaciones.usuario.perfilProfesional").FirstOrDefault(Publicacion => Publicacion.id == id),
                UsuarioActivo = context.Usuarios.FirstOrDefault(User => User.userName == conectUser),
                fechaActual=DateTime.Now
            };
           

           



            PubModel.pub.vista++;
            context.SaveChanges();

            //verificamos si existe postulacion del usuario
            Postulacion postulacion = context.Postulaciones.FirstOrDefault(o => o.idPublicacion == id && o.idUsuario == us.id);

            ViewBag.Postulo =  postulacion != null ? true : false;

            return View("Detalle", PubModel);

        }
        [HttpPost]
        public ViewResult Comentar(Comentario comentario)
        {

            actualizarPostulaciones();

            comentario.fecha = DateTime.Now;
            context.Comentarios.Add(comentario);
            context.SaveChanges();

            var comentarioSaved = comentario;
            Comentario com = context.Comentarios.Include("usuario").FirstOrDefault(o=>o.id== comentarioSaved.id); 

            return View("Comentar",com);

        }

        [HttpGet]
        public ActionResult Postulaciones(int id)
        {
            actualizarPostulaciones();


            var claim = HttpContext.User.Claims.First();
            string conectUser = claim.Value;


            PubModel PubModel = new PubModel
            {
                pub = context.Publicaciones.Include("usuario").Include("fotos").Include("postulaciones.usuario.perfilProfesional").Include("postulaciones.likesPostulacion").FirstOrDefault(Publicacion => Publicacion.id == id),
                UsuarioActivo = context.Usuarios.FirstOrDefault(User => User.userName == conectUser)

            };
   
            return View("Postulaciones", PubModel);

        }

        [HttpPost]
        public ViewResult FormReply (int idComentario,int idUsuario)
        {
            actualizarPostulaciones();


            Usuario us = context.Usuarios.FirstOrDefault(o=>o.id==idUsuario);
            ViewBag.idComent = idComentario;
            return View("FormReply", us);

        }
        [HttpPost]
        public ViewResult SendReply(ReplyComentario replyComentario)
        {
            actualizarPostulaciones();



            replyComentario.fecha = DateTime.Now;
            context.ReplyComentarios.Add(replyComentario);
            context.SaveChanges();
            ReplyComentario rc = context.ReplyComentarios.Include("usuario").FirstOrDefault(o=>o.id== replyComentario.id);
            return View("SendReply", replyComentario);

        }


        [HttpPost]
        public IActionResult setDonacion(Tarjeta t,Donacion donacion)
        {
            actualizarPostulaciones();


            Tarjeta tj = context.Tarjetas.FirstOrDefault(o => o.numero == t.numero && o.nombre == t.nombre && o.apellido == t.apellido && 
            o.mesVencimiento == t.mesVencimiento && o.anioVencimiento == t.anioVencimiento && o.codigoSeguridad==t.codigoSeguridad);

            if (tj==null)
            {
                ModelState.AddModelError("Mensaje","Verifique datos de tarjeta");
            }
            else if (donacion.monto >tj.saldo || tj.saldo==0)
            {
                ModelState.AddModelError("Mensaje", "Saldo insuficiente");
            }
            if (ModelState.IsValid)
            {
                tj.saldo -= donacion.monto;
                context.SaveChanges();
                donacion.fecha = DateTime.Now;
                
                context.Donaciones.Add(donacion);
                context.SaveChanges();
               


                var idPub = new { id = donacion.idPublicacion };
                 
                return RedirectToAction("Detalle", idPub);
            }

            return Detalle(donacion.idPublicacion);

        }



        [HttpPost]
        public IActionResult Postular(Postulacion postulacion,int cantidadPostulaciones)
        {
            actualizarPostulaciones();


            if (ModelState.IsValid)
            {
                postulacion.fechaCreacion = DateTime.Now;
                postulacion.estado = 0;

                context.Postulaciones.Add(postulacion);

                context.SaveChanges();


                if (cantidadPostulaciones==0)
                {
                    Publicacion publicacion = context.Publicaciones.FirstOrDefault(o=>o.id==postulacion.idPublicacion);
                    publicacion.limitePostulaciones = DateTime.Now.AddDays(7);
                    context.SaveChanges();
                }

          
            }


            return Detalle(postulacion.idPublicacion);

        }


        [HttpGet]
        public ViewResult TodasPublicaciones()
        {
            actualizarPostulaciones();

            var claim = HttpContext.User.Claims.First();
            string conectUser = claim.Value;


            IndexModel indexModel = new IndexModel
            {
                pubs = context.Publicaciones.Include("usuario").Include("fotos").Include("donaciones").Include("reacciones.usuario").Include("comentarios.usuario").Include("postulaciones.usuario.perfilProfesional").OrderByDescending(Publicacion => Publicacion.fechaCreacion).Where(o=>o.estado==0 || o.estado==3).ToList(),
                UsuarioActivo = context.Usuarios.FirstOrDefault(User => User.userName == conectUser),
                fechaActual = DateTime.Now
            };

            return View(indexModel);

        }



        [HttpGet]
        public ViewResult PubsCulminadas()
        {
            actualizarPostulaciones();

            var claim = HttpContext.User.Claims.First();
            string conectUser = claim.Value;


            IndexModel indexModel = new IndexModel
            {
                pubs = context.Publicaciones.Include("usuario").Include("fotos").Include("donaciones").Include("reacciones.usuario").Include("comentarios.usuario").Include("postulaciones.usuario.perfilProfesional").Include("postulaciones.evidencia.fotoEvidencias").OrderByDescending(Publicacion => Publicacion.fechaCreacion).Where(o => o.estado == 1).ToList(),
                UsuarioActivo = context.Usuarios.FirstOrDefault(User => User.userName == conectUser),
                fechaActual = DateTime.Now
            };
            

            return View("TodasPublicaciones", indexModel);

        }




        [HttpGet]
        public ViewResult MisPostulaciones()
        {

            actualizarPostulaciones();

            var claim = HttpContext.User.Claims.First();
            string conectUser = claim.Value;

            





            PostulacionModel postuModel = new PostulacionModel
            {
                
                UsuarioActivo = context.Usuarios.Include("postulaciones.publicacion").Include("postulaciones.publicacion.usuario").Include("postulaciones.publicacion.fotos").Include("postulaciones.publicacion.donaciones").Include("postulaciones.publicacion.reacciones.usuario").Include("postulaciones.publicacion.comentarios.usuario").Include("postulaciones.publicacion.postulaciones.usuario.perfilProfesional").FirstOrDefault(User => User.userName == conectUser),
                fechaActual = DateTime.Now
            };


            Console.WriteLine("Tiene postulaciones: "+postuModel.UsuarioActivo.postulaciones.Count());
            return View("MisPostulaciones", postuModel);

        }




        [HttpGet]
        public ViewResult MisAsignados()
        {

            actualizarPostulaciones();

            var claim = HttpContext.User.Claims.First();
            string conectUser = claim.Value;

            PostulacionModel postuModel = new PostulacionModel
            {

                UsuarioActivo = context.Usuarios.Include("postulaciones.publicacion").Include("postulaciones.publicacion.usuario").Include("postulaciones.publicacion.fotos").Include("postulaciones.publicacion.donaciones").Include("postulaciones.publicacion.reacciones.usuario").Include("postulaciones.publicacion.comentarios.usuario").Include("postulaciones.publicacion.postulaciones.usuario.perfilProfesional").FirstOrDefault(User => User.userName == conectUser),
                fechaActual = DateTime.Now
            };


          
            return View("MisAsignados", postuModel);

        }






        public List<Postulacion> ActualizarEstadoPostulaciones(int id)
        {

       


            DateTime fechaActual = DateTime.Now;
     
            List <Postulacion> postulaciones = context.Postulaciones.Include("likesPostulacion").Where(o=>o.idPublicacion==id ).ToList();

            int maxLikes = postulaciones.Max(o => o.likesPostulacion.Count());

            

            postulaciones = postulaciones.Where(o => o.likesPostulacion.Count()==maxLikes).ToList();

            DateTime fechaMin = postulaciones.Min(o=>o.fechaCreacion);

            Postulacion postulacion = postulaciones.FirstOrDefault(o => o.fechaCreacion == fechaMin);

            //logica para cambiar de estados segun situacion de publicaion y postulacion
            Publicacion publicacion = context.Publicaciones.FirstOrDefault(o => o.id == id);


            List<Donacion> donaciones = context.Donaciones.Where(o => o.idPublicacion == id).ToList();
            Double sumaDonaciones = donaciones.Sum(o => o.monto);

            Double montoRestante = 0;
            if (sumaDonaciones >= (Double)postulacion.monto)
            {
                montoRestante = sumaDonaciones - (Double)postulacion.monto;

                //ya tiene monto completo y trabajador asignado
                publicacion.estado = 3;
                publicacion.montoAsignado = (Double)postulacion.monto;
                postulacion.estado = 2;
                //envio de dinero restante a caja
                Caja caja = context.Cajas.FirstOrDefault();
                caja.monto += montoRestante;
            }
            if (sumaDonaciones < (Double)postulacion.monto)
            {
                publicacion.estado = 2;
                publicacion.montoAsignado = (Double)postulacion.monto;
                postulacion.estado = 1;

            }


            context.SaveChanges();

            List<Postulacion> postulacionesRemover = context.Postulaciones.Include("likesPostulacion").Where(o => o.idPublicacion == id && o.estado==0 ).ToList();

            //guardar lista
            List<Postulacion> posts = new List<Postulacion>();
            posts.AddRange(postulacionesRemover);

            context.Postulaciones.RemoveRange(postulacionesRemover);

            context.SaveChanges();


            return posts;


        }


        [HttpGet]
        public ViewResult cerrarTrabajo(int id)
        {
            actualizarPostulaciones();

            var claim = HttpContext.User.Claims.First();

            string conectUser = claim.Value;

            Usuario us = context.Usuarios.FirstOrDefault(o => o.userName == conectUser);
            PubModel PubModel = new PubModel
            {
                pub = context.Publicaciones.Include("usuario").Include("fotos").Include("donaciones.usuario").Include("reacciones.usuario").Include("comentarios.usuario").Include("comentarios.replyComentarios.usuario").Include("postulaciones.usuario.perfilProfesional").FirstOrDefault(Publicacion => Publicacion.id == id),
                UsuarioActivo = context.Usuarios.FirstOrDefault(User => User.userName == conectUser),
                fechaActual = DateTime.Now
            };

        

           

            return View("cerrarTrabajo", PubModel);


           
        }
        [HttpPost]
        public IActionResult cerrarTrabajo(Evidencia evidencia,List<IFormFile> files) {


            if (files.Count<6)
            {
                ModelState.AddModelError("evidencias","Se requiere almenos 6 fotos");
            }

            if (ModelState.IsValid)
            {
                context.Evidencias.Add(evidencia);
                context.SaveChanges();

                Postulacion postulacion = context.Postulaciones.FirstOrDefault(o=>o.id==evidencia.idPostulacion);
                postulacion.estado = 3;
                context.SaveChanges();

                Publicacion publicacion = context.Publicaciones.FirstOrDefault(o=>o.id==postulacion.idPublicacion);
                publicacion.estado = 1;
                context.SaveChanges();

                foreach (IFormFile file in files)
                {
                    FotoEvidencia f = new FotoEvidencia()
                    {

                        idEvidencia = evidencia.id,
                        contenido = SaveFile(file).Replace(" ", "%20")

                    };
                    context.FotoEvidencias.Add(f);
                    context.SaveChanges();
                }
                return RedirectToAction("Index","Home");
               
            }

            Postulacion pos = context.Postulaciones.FirstOrDefault(o => o.id == evidencia.idPostulacion);
         

            return cerrarTrabajo(pos.idPublicacion);
        }

        private string SaveFile(IFormFile file)
        {
            actualizarPostulaciones();

            string relativePath = "";

            if (file.Length > 0 && (file.ContentType == "image/png" || file.ContentType == "image/jpeg"))
            {
                relativePath = Path.Combine("files", "fotosEvidencias", file.FileName);
                var filePath = Path.Combine(hosting.WebRootPath, relativePath);
                var stream = new FileStream(filePath, FileMode.Create);
                file.CopyTo(stream);
                stream.Close();
            }

            return "/" + relativePath.Replace('\\', '/');
        }

        //actualizador


        public void actualizarPostulaciones()
        {




            List<Publicacion> publicaciones = context.Publicaciones.Where(o => o.estado == 0).ToList();

            foreach (Publicacion pub in publicaciones)
            {



                DateTime fechaActual = DateTime.Now;
                if (fechaActual >= pub.limitePostulaciones)
                {


                    List<Postulacion> postulaciones = context.Postulaciones.Include("likesPostulacion").Where(o => o.idPublicacion == pub.id).ToList();

                    int maxLikes = postulaciones.Max(o => o.likesPostulacion.Count());



                    postulaciones = postulaciones.Where(o => o.likesPostulacion.Count() == maxLikes).ToList();

                    DateTime fechaMin = postulaciones.Min(o => o.fechaCreacion);

                    Postulacion postulacion = postulaciones.FirstOrDefault(o => o.fechaCreacion == fechaMin);

                    //logica para cambiar de estados segun situacion de publicaion y postulacion
                    Publicacion publicacion = context.Publicaciones.FirstOrDefault(o => o.id == pub.id);


                    List<Donacion> donaciones = context.Donaciones.Where(o => o.idPublicacion == pub.id).ToList();
                    Double sumaDonaciones = donaciones.Sum(o => o.monto);

                    Double montoRestante = 0;
                    if (sumaDonaciones >= (Double)postulacion.monto)
                    {
                        montoRestante = sumaDonaciones - (Double)postulacion.monto;

                        //ya tiene monto completo y trabajador asignado
                        publicacion.estado = 3;
                        publicacion.montoAsignado = (Double)postulacion.monto;
                        postulacion.estado = 2;
                        //envio de dinero restante a caja
                        Caja caja = context.Cajas.FirstOrDefault();
                        caja.monto += montoRestante;
                    }
                    if (sumaDonaciones < (Double)postulacion.monto)
                    {
                        publicacion.estado = 2;
                        publicacion.montoAsignado = (Double)postulacion.monto;
                        postulacion.estado = 1;

                    }


                    context.SaveChanges();

                    List<Postulacion> postulacionesRemover = context.Postulaciones.Include("likesPostulacion").Where(o => o.idPublicacion == pub.id && o.estado == 0).ToList();

                    //guardar lista
                    List<Postulacion> posts = new List<Postulacion>();
                    posts.AddRange(postulacionesRemover);

                    context.Postulaciones.RemoveRange(postulacionesRemover);

                    context.SaveChanges();


                }






            }


        }


    }
}
