﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class Comentario
    {
        public int id { get; set; }
        public int idPublicacion { get; set; }
        public int idUsuario { get; set; }
        public DateTime fecha { get; set; }
        public string contenido { get; set; }
        public Usuario usuario { get; set; }
        public List<ReplyComentario> replyComentarios { get; set; }

    }
}
