﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class Postulacion
    {
        public int id { get; set; }
        public DateTime fechaCreacion { get; set; }

        [Required(ErrorMessage = "Monto requerido")]
        public Decimal monto { get; set; }
        [Required(ErrorMessage = "Dias requerido")]
        public int diasRealizacion { get; set; }

        public int estado { get; set; }
        [Required(ErrorMessage = "Descripcion requerida")]
        public string descripcion { get; set; }
        public int idUsuario { get; set; }
        public int idPublicacion { get; set; }
        public Usuario usuario { get; set; }
        public Publicacion publicacion { get; set; }

        public Evidencia evidencia { get; set; }
        public List<LikePostulacion> likesPostulacion { get; set; }
    }
}
