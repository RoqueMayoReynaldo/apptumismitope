﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class Tarjeta
    {
        public int id { get;set;}
        public int numero { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public int mesVencimiento { get; set; }
        public int anioVencimiento { get; set; }
        public int codigoSeguridad { get; set; }
        public Double saldo { get; set; }

    }
}
