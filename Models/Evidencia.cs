﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class Evidencia
    {
        public int id { get; set; }
        public int idPostulacion { get; set; }
        [Required(ErrorMessage ="Se requiere su descripcion")]
        public String descripcion { get; set; }
   
        public List<FotoEvidencia> fotoEvidencias { get; set; }
        
    }
}
